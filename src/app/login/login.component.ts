import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  formGroup: FormGroup;
  isSubmitted = false;
  isLoginSuccessfull = true;

  constructor(private authService: AuthService, private formBuilder: FormBuilder, private router: Router) { }

  ngOnInit(): void {
    this.formGroup = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  get formControls(): any {
    return this.formGroup.controls;
  }

  login(): void {
    this.isSubmitted = true;
    if (this.formGroup.valid) {
      const result = this.authService.login(this.formGroup.value);
      if (result) {
        this.router.navigateByUrl('/contacts');
      } else {
        this.isLoginSuccessfull = false;
      }
    }
    this.formGroup.reset();
  }

}
