import { Injectable } from '@angular/core';
import { Contact } from '../models/contact';

@Injectable({
  providedIn: 'root'
})
export class ContactService {

  private contacts: Contact[] = [
    {
      id: 1,
      firstName: 'Dragos',
      lastName: 'Popa',
      phoneNumber: '0755123123',
      email: 'dragos@gmail.com'
    },
    {
      id: 2,
      firstName: 'Nicoleta',
      lastName: 'Racasan',
      phoneNumber: '0746098983',
      email: 'nicoleta@gmail.com'
    },
    {
      id: 3,
      firstName: 'Florian',
      lastName: 'Stefanescu',
      phoneNumber: '0751789654',
      email: 'florian@gmail.com'
    },
    {
      id: 4,
      firstName: 'Emil',
      lastName: 'Teodor',
      phoneNumber: '0756678090',
      email: 'emil@gmail.com'
    },
  ];

  constructor() { }

  getAll(): Contact[] {
    return this.contacts;
  }

  add(contact: Contact): void {
    if (this.contacts.length > 0) {
      const lastId = this.contacts.reverse()[0].id;
      contact.id = lastId + 1;
    } else {
      contact.id = 1;
    }
    this.contacts.push(contact);
  }

  remove(id: number): void {
    this.contacts.splice(this.contacts.findIndex(el => el.id === id), 1);
  }

  edit(contact: Contact): void {
    const foundContactIdx = this.contacts.findIndex(el => el.id === contact.id);
    // remove contact and add updated one in its place
    this.contacts.splice(foundContactIdx, 1, contact);
  }


}
