import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private allowedUsers: User[] = [
    { email: 'unu@unu.unu', password: 'unu' },
    { email: 'doi@doi.doi', password: 'doi' }
  ];

  constructor() { }

  public login(user: User): boolean {
    if (this.allowedUsers.find(el => el.email === user.email && el.password === user.password)) {
      localStorage.setItem('TOKEN', 'my_token');
      return true;
    } else {
      return false;
    }
  }

  public logout(): void {
    localStorage.removeItem('TOKEN');
  }

  public isLoggedIn(): boolean {
    return localStorage.getItem('TOKEN') !== null;
  }
}
