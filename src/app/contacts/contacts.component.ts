import { Component, OnInit } from '@angular/core';
import { ContactService } from '../services/contact.service';
import { Contact } from '../models/contact';
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { ContactDialogComponent } from '../contact-dialog/contact-dialog.component';
import { AuthService } from '../services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  styleUrls: ['./contacts.component.css']
})
export class ContactsComponent implements OnInit {

  displayedColumns: string[] = ['firstName', 'lastName', 'phoneNumber', 'email', 'actions'];
  contacts: MatTableDataSource<Contact>;

  constructor(
    private contactService: ContactService,
    private matDialog: MatDialog,
    private authService: AuthService,
    private router: Router) { }

  ngOnInit(): void {
    this.getAll();
  }

  add(): void {
    const dialogRef = this.matDialog.open(ContactDialogComponent);
    dialogRef.afterClosed().subscribe(res => {
      if (res !== undefined) {
        this.contactService.add(res as Contact);
        this.getAll();
      }
    });
  }

  edit(contact: Contact): void {
    const dialogRef = this.matDialog.open(ContactDialogComponent, {
      data: contact
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res !== undefined) {
        // preserve the id
        const newContact = res as Contact;
        newContact.id = contact.id;
        this.contactService.edit(newContact);
        this.getAll();
      }
    });
  }

  remove(id: number): void {
    this.contactService.remove(id);
    this.getAll();
  }

  getAll(): void {
    this.contacts = new MatTableDataSource(this.contactService.getAll());
  }

  logout(): void {
    this.authService.logout();
    this.router.navigateByUrl('/login');
  }

}
